Player.addItemCreativeInv(152, 1);
 
Player.addItemCreativeInv(33, 1);
 
Player.addItemCreativeInv(28, 1);
 
//functions
function useItem(x,y,z,i,b,s){
	if(getCarriedItem()==33){
		if(s==0){
			setTile(x,y-1,z,33,0);
	}
	if(s==1){
		setTile(x,y+1,z,33,1);
    
	}
	}
}
 
function destroyBlock(x, y, z){
	if(getTile(x, y, z) == 33){
		preventDefault();
		Level.destroyBlock(x, y, z, false);
		if(Level.getGameMode() == 0) {
			Level.dropItem(x, y, z, 0, 33, 1, 0);
		} 
		else(Level.getGameMode() == 1);
}

	if(getTile(x, y, z) == 28){
		preventDefault();
		Level.destroyBlock(x, y, z, false);
		if(Level.getGameMode() == 0){
			Level.dropItem(x, y, z, 0, 28, 1, 0);
		} 
		else(Level.getGameMode() == 1);
	}
	if(getTile(x, y, z) == 152){
		Level.addParticle(ParticleType.redstone, x+1, y, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x-1, y, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y+1, z, 0, 0, 0, 4);
		Level.addParticle(ParticleType.redstone, x, y-1, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y, z-1, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y, z+1, 0, 0, 0, 2); 
	}
//Lamp off
	if(getTile(x, y, z) == 123){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
		if(Level.getGameMode() == 0){
			Level.dropItem(x, y, z, 0, 123, 1, 0);
		}
		else(Level.getGameMode() == 1);
	}
//Lamp on
	if(getTile(x, y, z) == 124){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
    
	}
}
//Destroy
Block.setDestroyTime(33, 0.75);
Block.setDestroyTime(28, 0.75);