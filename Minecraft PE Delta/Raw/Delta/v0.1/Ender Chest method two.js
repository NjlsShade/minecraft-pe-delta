//Mod by Kaghon. Idea from Platypus979. 
//Message Me on Twitter @androidkaghon
//Do not redistribute or modify without permission!
//Enjoy!

var enderChestId=130;
var chestStage=0;
var itemCount=null;
var enderx=null;
var endery=null;
var enderz=null;
var chestSlotId = 0;
var chestSlotCount=0;
var chestSlotData=0;
var chestSlotId1=0;
var chestSlotCount1=0;
var chestSlotData1=0;
var chestSlotId2=0;
var chestSlotCount2=0;
var chestSlotData2=0;
var chestSlotId3=0;
var chestSlotCount3=0;
var chestSlotData3=0;
var chestSlotId4=0;
var chestSlotCount4=0;
var chestSlotData4=0;
var chestSlotId5=0;
var chestSlotCount5=0;
var chestSlotData5=0;
var chestSlotId6=0;
var chestSlotCount6=0;
var chestSlotData6=0;
var chestSlotId7=0;
var chestSlotCount7=0;
var chestSlotData7=0;
var chestSlotId8=0;
var chestSlotCount8=0;
var chestSlotData8=0;
var chestSlotId9=0;
var chestSlotCount9=0;
var chestSlotData9=0;
var chestSlotId10=0;
var chestSlotCount10=0;
var chestSlotData10=0;
var chestSlotId11=0;
var chestSlotCount11=0;
var chestSlotData11=0;
var chestSlotId12=0;
var chestSlotCount12=0;
var chestSlotData12=0;
var chestSlotId13=0;
var chestSlotCount13=0;
var chestSlotData13=0;
var chestSlotId14=0;
var chestSlotCount14=0;
var chestSlotData14=0;
var chestSlotId15=0;
var chestSlotCount15=0;
var chestSlotData15=0;
var chestSlotId16=0;
var chestSlotCount16=0;
var chestSlotData16=0;
var chestSlotId17=0;
var chestSlotCount17=0;
var chestSlotData17=0;
var chestSlotId18=0;
var chestSlotCount18=0;
var chestSlotData18=0;
var chestSlotId19=0;
var chestSlotCount19=0;
var chestSlotData19=0;
var chestSlotId20=0;
var chestSlotCount20=0;
var chestSlotData20=0;
var chestSlotId21=0;
var chestSlotCount21=0;
var chestSlotData21=0;
var chestSlotId22=0;
var chestSlotCount22=0;
var chestSlotData22=0;
var chestSlotId23=0;
var chestSlotCount23=0;
var chestSlotData23=0;
var chestSlotId24=0;
var chestSlotCount24=0;
var chestSlotData24=0;
var chestSlotId25=0;
var chestSlotCount25=0;
var chestSlotData25=0;
var chestSlotId26=0;
var chestSlotCount26=0;
var chestSlotData26=0;
var enderEye=null;
var yaw;
var pitch;
var dirx;
var diry;
var dirz;
var eye=false;
var GUI; 
var showbttn=0;
var amount;

Player.addItemCreativeInv(381,1,0);

ModPE.setItem(381,"ender_pearl",0,"Ender Pearl");
ModPE.setItem(377,"blaze_powder",0,"Blaze Powder");
ModPE.setItem(382,"ender_eye",0,"Ender Eye");

Item.addShapedRecipe(130,1,0,[
"ooo",
"oeo",
"ooo"],[
"o",49,0,
"e",382,0]);

Item.addShapedRecipe(382,1,0,[
" b ",
" p ",
"   "],[
"b",377,0,
"p",381,0]);
           
Block.defineBlock(enderChestId, "Ender Chest", [["obsidian", 0], ["obsidian" , 0], ["endframee" , 0], ["endframee", 0], ["endframee", 0], ["endframee", 0]]);
Block.setDestroyTime(enderChestId, 22.5);

function addMobToRenderer(renderer){ 
var model=renderer.getModel();
var bipedBody = model.getPart("body").clear(); 
var bipedLeg = model.getPart("rightLeg").clear(); 
var bipedLeg2 = model.getPart("leftLeg").clear(); 
var bipedhead = model.getPart("head").clear(); 
var bipedarm = model.getPart("rightArm").clear(); 
var bipedarm2 = model.getPart("leftArm").clear();
bipedhead.addBox(0, 0, 0, 4, 4, 4, 0)
}

var Renderer = Renderer.createHumanoidRenderer(); addMobToRenderer(Renderer);

function entityRemovedHook(entity){
if(entity==enderEye){
preventDefault();
setPosition(getPlayerEnt(), Entity.getX(entity), Entity.getY(entity)+2, Entity.getZ(entity));
}
}


function procCmd(cmd){
switch(cmd){
case"ender chest":
addItemInventory(130, 64);
clientMessage("Given 64 ender chests");
break;
case"ec":
addItemInventory(130, 64);
clientMessage("Given 64 ender chests");
break;
case"reset":
ModPE.resetImages();
break;
}
}

function deathHook(attacker, victim){
if(Entity.getEntityTypeId(victim)==38){
Level.dropItem(Entity.getX(victim), Entity.getY(victim),
Entity.getZ(victim), 1, 381, 1, 0);
}
if(Entity.getEntityTypeId(victim)==36){
Level.dropItem(Entity.getX(victim), Entity.getY(victim),
Entity.getZ(victim), 1, 377, 1, 0);
}
}

function useItem(x, y, z, itemId, blockId, side) {
if(blockId==enderChestId&&chestStage==0&&getTile(x,y+1,z)==0) {
enderx=x;
endery=y;
enderz=z;
setTile(x, y, z, 54);
chestStage = 1;
Level.setChestSlot(enderx, endery, enderz, 0, chestSlotId, chestSlotData, chestSlotCount);
Level.setChestSlot(enderx, endery, enderz, 1, chestSlotId1, chestSlotData1, chestSlotCount1);
Level.setChestSlot(enderx, endery, enderz, 2, chestSlotId2, chestSlotData2, chestSlotCount2);
Level.setChestSlot(enderx, endery, enderz, 3, chestSlotId3, chestSlotData3, chestSlotCount3);
Level.setChestSlot(enderx, endery, enderz, 4, chestSlotId4, chestSlotData4, chestSlotCount4);
Level.setChestSlot(enderx, endery, enderz, 5, chestSlotId5, chestSlotData5, chestSlotCount5);
Level.setChestSlot(enderx, endery, enderz, 6, chestSlotId6, chestSlotData6, chestSlotCount6);
Level.setChestSlot(enderx, endery, enderz, 7, chestSlotId7, chestSlotData7, chestSlotCount7);
Level.setChestSlot(enderx, endery, enderz, 8, chestSlotId8, chestSlotData8, chestSlotCount8);
Level.setChestSlot(enderx, endery, enderz, 9, chestSlotId9, chestSlotData9, chestSlotCount9);
Level.setChestSlot(enderx, endery, enderz, 10, chestSlotId10, chestSlotData10, chestSlotCount10);
Level.setChestSlot(enderx, endery, enderz, 11, chestSlotId11, chestSlotData11, chestSlotCount11);
Level.setChestSlot(enderx, endery, enderz, 12, chestSlotId12, chestSlotData12, chestSlotCount12);
Level.setChestSlot(enderx, endery, enderz, 13, chestSlotId13, chestSlotData13, chestSlotCount13);
Level.setChestSlot(enderx, endery, enderz, 14, chestSlotId14, chestSlotData14, chestSlotCount14);
Level.setChestSlot(enderx, endery, enderz, 15, chestSlotId15, chestSlotData15, chestSlotCount15);
Level.setChestSlot(enderx, endery, enderz, 16, chestSlotId16, chestSlotData16, chestSlotCount16);
Level.setChestSlot(enderx, endery, enderz, 17, chestSlotId17, chestSlotData17, chestSlotCount17);
Level.setChestSlot(enderx, endery, enderz, 18, chestSlotId18, chestSlotData18, chestSlotCount18);
Level.setChestSlot(enderx, endery, enderz, 19, chestSlotId19, chestSlotData19, chestSlotCount19);
Level.setChestSlot(enderx, endery, enderz, 20, chestSlotId20, chestSlotData20, chestSlotCount20);
Level.setChestSlot(enderx, endery, enderz, 21, chestSlotId21, chestSlotData21, chestSlotCount21);
Level.setChestSlot(enderx, endery, enderz, 22, chestSlotId22, chestSlotData22, chestSlotCount22);
Level.setChestSlot(enderx, endery, enderz, 23, chestSlotId23, chestSlotData23, chestSlotCount23);
Level.setChestSlot(enderx, endery, enderz, 24, chestSlotId24, chestSlotData24, chestSlotCount24);
Level.setChestSlot(enderx, endery, enderz, 25, chestSlotId25, chestSlotData25, chestSlotCount25);
Level.setChestSlot(enderx, endery, enderz, 26, chestSlotId26, chestSlotData26, chestSlotCount26);
}
}


function blockEventHook(x, y, z,eventType,data){
if(x==enderx&&y==endery&&z==enderz&&chestStage==1&&data==1){
chestStage=2;
}
else if(x==enderx&&y==endery&&z==enderz&&chestStage==2&&data==0){
preventDefault();
chestSlotId= Level.getChestSlot(enderx, endery, enderz, 0);
chestSlotCount= Level.getChestSlotCount(enderx, endery, enderz, 0);
chestSlotData= Level.getChestSlotData(enderx, endery, enderz, 0);
chestSlotId1= Level.getChestSlot(enderx, endery, enderz, 1);
chestSlotCount1= Level.getChestSlotCount(enderx, endery, enderz, 1);
chestSlotData1= Level.getChestSlotData(enderx, endery, enderz, 1);
chestSlotId2= Level.getChestSlot(enderx, endery, enderz, 2);
chestSlotCount2= Level.getChestSlotCount(enderx, endery, enderz, 2);
chestSlotData2= Level.getChestSlotData(enderx, endery, enderz, 2);
chestSlotId3= Level.getChestSlot(enderx, endery, enderz, 3);
chestSlotCount3= Level.getChestSlotCount(enderx, endery, enderz, 3);
chestSlotData3= Level.getChestSlotData(enderx, endery, enderz, 3);
chestSlotId4= Level.getChestSlot(enderx, endery, enderz, 4);
chestSlotCount4= Level.getChestSlotCount(enderx, endery, enderz, 4);
chestSlotData4= Level.getChestSlotData(enderx, endery, enderz, 4);
chestSlotId5= Level.getChestSlot(enderx, endery, enderz, 5);
chestSlotCount5= Level.getChestSlotCount(enderx, endery, enderz, 5);
chestSlotData5= Level.getChestSlotData(enderx, endery, enderz, 5);
chestSlotId6= Level.getChestSlot(enderx, endery, enderz, 6);
chestSlotCount6= Level.getChestSlotCount(enderx, endery, enderz, 6);
chestSlotData6= Level.getChestSlotData(enderx, endery, enderz, 6);
chestSlotId7= Level.getChestSlot(enderx, endery, enderz, 7);
chestSlotCount7= Level.getChestSlotCount(enderx, endery, enderz, 7);
chestSlotData7= Level.getChestSlotData(enderx, endery, enderz, 7);
chestSlotId8= Level.getChestSlot(enderx, endery, enderz, 8);
chestSlotCount8= Level.getChestSlotCount(enderx, endery, enderz, 8);
chestSlotData8= Level.getChestSlotData(enderx, endery, enderz, 8);
chestSlotId9= Level.getChestSlot(enderx, endery, enderz, 9);
chestSlotCount9= Level.getChestSlotCount(enderx, endery, enderz, 9);
chestSlotData9= Level.getChestSlotData(enderx, endery, enderz, 9);
chestSlotId10= Level.getChestSlot(enderx, endery, enderz, 10);
chestSlotCount10= Level.getChestSlotCount(enderx, endery, enderz, 10);
chestSlotData10= Level.getChestSlotData(enderx, endery, enderz, 10);
chestSlotId11= Level.getChestSlot(enderx, endery, enderz, 11);
chestSlotCount11= Level.getChestSlotCount(enderx, endery, enderz, 11);
chestSlotData11= Level.getChestSlotData(enderx, endery, enderz, 11);
chestSlotId12= Level.getChestSlot(enderx, endery, enderz, 12);
chestSlotCount12= Level.getChestSlotCount(enderx, endery, enderz, 12);
chestSlotData12= Level.getChestSlotData(enderx, endery, enderz, 12);
chestSlotId13= Level.getChestSlot(enderx, endery, enderz, 13);
chestSlotCount13= Level.getChestSlotCount(enderx, endery, enderz, 13);
chestSlotData13= Level.getChestSlotData(enderx, endery, enderz, 13);
chestSlotId14= Level.getChestSlot(enderx, endery, enderz, 14);
chestSlotCount14= Level.getChestSlotCount(enderx, endery, enderz, 14);
chestSlotData14= Level.getChestSlotData(enderx, endery, enderz, 14);
chestSlotId15= Level.getChestSlot(enderx, endery, enderz, 15);
chestSlotCount15= Level.getChestSlotCount(enderx, endery, enderz, 15);
chestSlotData15= Level.getChestSlotData(enderx, endery, enderz, 15);
chestSlotId16= Level.getChestSlot(enderx, endery, enderz, 16);
chestSlotCount16= Level.getChestSlotCount(enderx, endery, enderz, 16);
chestSlotData16= Level.getChestSlotData(enderx, endery, enderz, 16);
chestSlotId17= Level.getChestSlot(enderx, endery, enderz, 17);
chestSlotCount17= Level.getChestSlotCount(enderx, endery, enderz, 17);
chestSlotData17= Level.getChestSlotData(enderx, endery, enderz, 17);
chestSlotId18= Level.getChestSlot(enderx, endery, enderz, 18);
chestSlotCount18= Level.getChestSlotCount(enderx, endery, enderz, 18);
chestSlotData18= Level.getChestSlotData(enderx, endery, enderz, 18);
chestSlotId19= Level.getChestSlot(enderx, endery, enderz, 19);
chestSlotCount19= Level.getChestSlotCount(enderx, endery, enderz, 19);
chestSlotData19= Level.getChestSlotData(enderx, endery, enderz, 19);
chestSlotId20= Level.getChestSlot(enderx, endery, enderz, 20);
chestSlotCount20= Level.getChestSlotCount(enderx, endery, enderz, 20);
chestSlotData20= Level.getChestSlotData(enderx, endery, enderz, 20);
chestSlotId21= Level.getChestSlot(enderx, endery, enderz, 21);
chestSlotCount21= Level.getChestSlotCount(enderx, endery, enderz, 21);
chestSlotData21= Level.getChestSlotData(enderx, endery, enderz, 21);
chestSlotId22= Level.getChestSlot(enderx, endery, enderz, 22);
chestSlotCount22= Level.getChestSlotCount(enderx, endery, enderz, 22);
chestSlotData22= Level.getChestSlotData(enderx, endery, enderz, 22);
chestSlotId23= Level.getChestSlot(enderx, endery, enderz, 23);
chestSlotCount23= Level.getChestSlotCount(enderx, endery, enderz, 23);
chestSlotData23= Level.getChestSlotData(enderx, endery, enderz, 23);
chestSlotId24= Level.getChestSlot(enderx, endery, enderz, 24);
chestSlotCount24= Level.getChestSlotCount(enderx, endery, enderz, 24);
chestSlotData24= Level.getChestSlotData(enderx, endery, enderz, 24);
chestSlotId25= Level.getChestSlot(enderx, endery, enderz, 25);
chestSlotCount25= Level.getChestSlotCount(enderx, endery, enderz, 25);
chestSlotData25= Level.getChestSlotData(enderx, endery, enderz, 25);
chestSlotId26= Level.getChestSlot(enderx, endery, enderz, 26);
chestSlotCount26= Level.getChestSlotCount(enderx, endery, enderz, 26);
chestSlotData26= Level.getChestSlotData(enderx, endery, enderz, 26);
for(s=0; s<27; s++){
Level.setChestSlot(enderx, endery, enderz, s, 0, 0, 0);
}
setTile(enderx, endery, enderz, enderChestId);
chestStage=0;
}
}

function modTick(){
if(enderEye!=null&&getTile(Math.round(Entity.getX(enderEye)), Math.round(Entity.getY(enderEye))-1, Math.round(Entity.getZ(enderEye)))!=0||getTile(Math.round(Entity.getX(enderEye))+1, Math.round(Entity.getY(enderEye)), Math.round(Entity.getZ(enderEye)))!=0|| getTile(Math.round(Entity.getX(enderEye))-1, Math.round(Entity.getY(enderEye)), Math.round(Entity.getZ(enderEye)))!=0||getTile(Math.round(Entity.getX(enderEye)), Math.round(Entity.getY(enderEye)), Math.round(Entity.getZ(enderEye))+1)!=0||getTile(Math.round(Entity.getX(enderEye)), Math.round(Entity.getY(enderEye)), Math.round(Entity.getZ(enderEye))-1)!=0 ){
preventDefault();
Entity.remove(enderEye);
}
if(eye==true){
pitch = ((Entity.getPitch(getPlayerEnt()) + 90) * Math.PI)/180;
yaw = ((Entity.getYaw(getPlayerEnt()) + 90) * Math.PI)/180;
dirx = Math.sin(pitch) * Math.cos(yaw);
diry = Math.sin(pitch) * Math.sin(yaw);
dirz = Math.cos(pitch);
enderEye=Level.spawnMob(Player.getX()+dirx,Player.getY()+dirz,Player.getZ()+diry ,15,"mob/eye.png");
Entity.setRenderType(enderEye, Renderer.renderType);
Entity.setHealth(enderEye, 2);
setVelX(enderEye,4*dirx); 
setVelY(enderEye,4*dirz); 
setVelZ(enderEye,4*diry);
eye=false;
}
if(getCarriedItem()==381&&showbttn!=1){
showbttn=1;
var ctx = com.mojang.minecraftpe.MainActivity.currentMainActivity.get(); 
  ctx.runOnUiThread(new java.lang.Runnable({ run: function(){ 
    try{ 
      var layout = new android.widget.LinearLayout(ctx); 
      layout.setOrientation(1); 

      var button = new android.widget.Button(ctx); 
      button.setText("Throw Ender Pearl"); 
      button.setWidth(200);
      button.setHeight(200);
      button.setOnClickListener(new android.view.View.OnClickListener({ 
        onClick: function(viewarg){ 
if(showbttn==1){ 
eye=true;
if(Level.getGameMode()==0){
amount=Player.getCarriedItemCount();
Entity.setCarriedItem(getPlayerEnt(), 381, amount-1, 0, 0);
if(amount==1){
Entity.setCarriedItem(getPlayerEnt(), 0, 0, 0, 0);
}
}
GUI.dismiss();
showbttn=0;
}  
        } 
      })); 
      layout.addView(button); 

      GUI = new android.widget.PopupWindow(layout, android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT, android.widget.RelativeLayout.LayoutParams.WRAP_CONTENT); 
      GUI.setBackgroundDrawable(new android.graphics.drawable.ColorDrawable(android.graphics.Color.TRANSPARENT));     
      GUI.showAtLocation(ctx.getWindow().getDecorView(), android.view.Gravity.RIGHT | android.view.Gravity.BOTTOM, 0, 300); 
    }catch(err){ 
      print("An error occured: " + err); 
    } 
}}));
}
if(getCarriedItem()!=381){
showbttn=0;
var ctx = com.mojang.minecraftpe.MainActivity.currentMainActivity.get(); ctx.runOnUiThread(new java.lang.Runnable({ run: function(){
if(GUI != null){ 
GUI.dismiss(); 
GUI = null;
} }})); 
}   
}

function leaveGame(){
var ctx = com.mojang.minecraftpe.MainActivity.currentMainActivity.get(); ctx.runOnUiThread(new java.lang.Runnable({ run: function(){
if(GUI != null){ 
GUI.dismiss(); 
GUI = null;
} }})); 
ModPE.saveData("id0", chestSlotId);
ModPE.saveData("count0", chestSlotCount);
ModPE.saveData("data0", chestSlotData);
ModPE.saveData("id1", chestSlotId1);
ModPE.saveData("count1", chestSlotCount1);
ModPE.saveData("data1", chestSlotData1);
ModPE.saveData("id2", chestSlotId2);
ModPE.saveData("count2", chestSlotCount2);
ModPE.saveData("data2", chestSlotData2);
ModPE.saveData("id3", chestSlotId3);
ModPE.saveData("count3", chestSlotCount3);
ModPE.saveData("data3", chestSlotData3);
ModPE.saveData("id4", chestSlotId4);
ModPE.saveData("count4", chestSlotCount4);
ModPE.saveData("data4", chestSlotData4);
ModPE.saveData("id5", chestSlotId5);
ModPE.saveData("count5", chestSlotCount5);
ModPE.saveData("data5", chestSlotData5);
ModPE.saveData("id6", chestSlotId6);
ModPE.saveData("count6", chestSlotCount6);
ModPE.saveData("data6", chestSlotData6);
ModPE.saveData("id7", chestSlotId7);
ModPE.saveData("count7", chestSlotCount7);
ModPE.saveData("data7", chestSlotData7);
ModPE.saveData("id8", chestSlotId8);
ModPE.saveData("count8", chestSlotCount8);
ModPE.saveData("data8", chestSlotData8);
ModPE.saveData("id9", chestSlotId9);
ModPE.saveData("count9", chestSlotCount9);
ModPE.saveData("data9", chestSlotData9);
ModPE.saveData("id10", chestSlotId10);
ModPE.saveData("count10", chestSlotCount10);
ModPE.saveData("data10", chestSlotData10);
ModPE.saveData("id11", chestSlotId11);
ModPE.saveData("count11", chestSlotCount11);
ModPE.saveData("data11", chestSlotData11);
ModPE.saveData("id12", chestSlotId12);
ModPE.saveData("count12", chestSlotCount12);
ModPE.saveData("data12", chestSlotData12);
ModPE.saveData("id13", chestSlotId13);
ModPE.saveData("count13", chestSlotCount13);
ModPE.saveData("data13", chestSlotData13);
ModPE.saveData("id14", chestSlotId14);
ModPE.saveData("count14", chestSlotCount14);
ModPE.saveData("data14", chestSlotData14);
ModPE.saveData("id15", chestSlotId15);
ModPE.saveData("count15", chestSlotCount15);
ModPE.saveData("data15", chestSlotData15);
ModPE.saveData("id16", chestSlotId16);
ModPE.saveData("count16", chestSlotCount16);
ModPE.saveData("data16", chestSlotData16);
ModPE.saveData("id17", chestSlotId17);
ModPE.saveData("count17", chestSlotCount17);
ModPE.saveData("data17", chestSlotData17);
ModPE.saveData("id18", chestSlotId18);
ModPE.saveData("count18", chestSlotCount18);
ModPE.saveData("data18", chestSlotData18);
ModPE.saveData("id19", chestSlotId19);
ModPE.saveData("count19", chestSlotCount19);
ModPE.saveData("data19", chestSlotData19);
ModPE.saveData("id20", chestSlotId20);
ModPE.saveData("count20", chestSlotCount20);
ModPE.saveData("data20", chestSlotData20);
ModPE.saveData("count21", chestSlotCount21);
ModPE.saveData("data21", chestSlotData21);
ModPE.saveData("id22", chestSlotId22);
ModPE.saveData("count22", chestSlotCount22);
ModPE.saveData("data22", chestSlotData22);
ModPE.saveData("id23", chestSlotId23);
ModPE.saveData("count23", chestSlotCount23);
ModPE.saveData("data23", chestSlotData23);
ModPE.saveData("id24", chestSlotId24);
ModPE.saveData("count24", chestSlotCount24);
ModPE.saveData("data24", chestSlotData24);
ModPE.saveData("id25", chestSlotId25);
ModPE.saveData("count25", chestSlotCount25);
ModPE.saveData("data25", chestSlotData25);
ModPE.saveData("id26", chestSlotId26);
ModPE.saveData("count26", chestSlotCount26);
ModPE.saveData("data26", chestSlotData26);
}

function newLevel(){
chestSlotId = ModPE.readData("id0");
chestSlotCount = ModPE.readData("count0");
chestSlotData = ModPE.readData("data0");
chestSlotId1 = ModPE.readData("id1");
chestSlotCount1 = ModPE.readData("count1");
chestSlotData1 = ModPE.readData("data1");
chestSlotId2 = ModPE.readData("id2");
chestSlotCount2 = ModPE.readData("count2");
chestSlotData2 = ModPE.readData("data2");
chestSlotId3 = ModPE.readData("id3");
chestSlotCount3 = ModPE.readData("count3");
chestSlotData3 = ModPE.readData("data3");
chestSlotId4 = ModPE.readData("id4");
chestSlotCount4 = ModPE.readData("count4");
chestSlotData4 = ModPE.readData("data4");
chestSlotId5 = ModPE.readData("id5");
chestSlotCount5 = ModPE.readData("count5");
chestSlotData5 = ModPE.readData("data5");
chestSlotId6 = ModPE.readData("id6");
chestSlotCount6 = ModPE.readData("count6");
chestSlotData6 = ModPE.readData("data6");
chestSlotId7 = ModPE.readData("id7");
chestSlotCount7 = ModPE.readData("count7");
chestSlotData7 = ModPE.readData("data7");
chestSlotId8 = ModPE.readData("id8");
chestSlotCount8 = ModPE.readData("count8");
chestSlotData8 = ModPE.readData("data8");
chestSlotId9 = ModPE.readData("id9");
chestSlotCount9 = ModPE.readData("count9");
chestSlotData9 = ModPE.readData("data9");
chestSlotId10 = ModPE.readData("id10");
chestSlotCount10 = ModPE.readData("count10");
chestSlotData10 = ModPE.readData("data10");
chestSlotId11 = ModPE.readData("id11");
chestSlotCount11 = ModPE.readData("count11");
chestSlotData11 = ModPE.readData("data11");
chestSlotId12 = ModPE.readData("id12");
chestSlotCount12 = ModPE.readData("count12");
chestSlotData12 = ModPE.readData("data12");
chestSlotId13 = ModPE.readData("id13");
chestSlotCount13 = ModPE.readData("count13");
chestSlotData13 = ModPE.readData("data13");
chestSlotId14 = ModPE.readData("id14");
chestSlotCount14 = ModPE.readData("count14");
chestSlotData14 = ModPE.readData("data14");
chestSlotId15 = ModPE.readData("id15");
chestSlotCount15 = ModPE.readData("count15");
chestSlotData15 = ModPE.readData("data15");
chestSlotId16 = ModPE.readData("id16");
chestSlotCount16 = ModPE.readData("count16");
chestSlotData16 = ModPE.readData("data16");
chestSlotId17 = ModPE.readData("id17");
chestSlotCount17 = ModPE.readData("count17");
chestSlotData17 = ModPE.readData("data17");
chestSlotId18 = ModPE.readData("id18");
chestSlotCount18 = ModPE.readData("count18");
chestSlotData18 = ModPE.readData("data18");
chestSlotId19 = ModPE.readData("id19");
chestSlotCount19 = ModPE.readData("count19");
chestSlotData19 = ModPE.readData("data19");
chestSlotId20 = ModPE.readData("id20");
chestSlotCount20 = ModPE.readData("count20");
chestSlotData20 = ModPE.readData("data20");
chestSlotId21 = ModPE.readData("id21");
chestSlotCount21 = ModPE.readData("count21");
chestSlotData21 = ModPE.readData("data21");
chestSlotId22 = ModPE.readData("id22");
chestSlotCount22 = ModPE.readData("count22");
chestSlotData22 = ModPE.readData("data22");
chestSlotId23 = ModPE.readData("id23");
chestSlotCount23 = ModPE.readData("count23");
chestSlotData23 = ModPE.readData("data23");
chestSlotId24 = ModPE.readData("id24");
chestSlotCount24 = ModPE.readData("count24");
chestSlotData24 = ModPE.readData("data24");
chestSlotId25 = ModPE.readData("id25");
chestSlotCount25 = ModPE.readData("count25");
chestSlotData25 = ModPE.readData("data25");
chestSlotId26 = ModPE.readData("id26");
chestSlotCount26 = ModPE.readData("count26");
chestSlotData26 = ModPE.readData("data26");
}








