//Variables
var eb = 0;
var ex = 0;
var ey = 0;
var ez = 0;
var t = 0;
var currentSlot = 0;

var nOfJ = 0;

var eId = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var eData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var eCount = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

//Now Playing message variables
var nowPlayingMessageBool = false;
var countdownForNowPlayingMessage = 0;
var timesChanged = 0;
var currentColor = 0;
var nowPlayingMessage = "";

//Directory variables
var pathsave = android.os.Environment.getExternalStorageDirectory().getPath() + "/games/com.mojang/minecraftworlds/" + Level.getWorldName() + "/";
const MAX_LOGARITHMIC_VOLUME = 65;
const JUKEBOX_SONGS_PATH = (new android.os.Environment.getExternalStorageDirectory() + "/games/com.mojang/minecraft-jukebox/");
var initCreativeItems = true;
var jukeboxes = [];
var discNames = ["13 Disc", "Cat Disc", "Blocks Disc", "Chirp Disc", "Far Disc", "Mall Disc", "Mellohi Disc", "Stal Disc", "Strad Disc", "Ward Disc", "11 Disc", "Wait Disc"];
var discFiles = ["13.ogg", "cat.ogg", "blocks.ogg", "chirp.ogg", "far.ogg", "mall.ogg", "mellohi.ogg", "stal.ogg", "strad.ogg", "ward.ogg", "11.ogg", "wait.ogg"];
var currentActivity = com.mojang.minecraftpe.MainActivity.currentMainActivity.get();

//BETA
//var useable=true;
//var tick=0;

//ID-----------------------------------------------------------

//Blocks

//Detector Rail
Player.addItemCreativeInv(28,1,0);
//Piston
Player.addItemCreativeInv(33,1,0);
//Ender Chest
Player.addItemCreativeInv(130,1,0);
//Redstone block
Player.addItemCreativeInv(152,1,0);
//Eye of Ender
Player.addItemCreativeInv(381,1,0);
//Ender Pearl
Player.addItemCreativeInv(368,1,0);
//Blaze Powder
Player.addItemCreativeInv(377,1,0);
//Cave Spider
//Player.addItemCreativeInv(383,1,40);
//Ghast
//Player.addItemCreativeInv(383,1,41);
//Magma Cube
//Player.addItemCreativeInv(383,1,42);
//White Glass
Player.addItemCreativeInv(176,1,0)
//Orange Glass
Player.addItemCreativeInv(177,1,0)
//Magenta Glass
Player.addItemCreativeInv(178,1,0)
//Light Blue Glass
Player.addItemCreativeInv(179,1,0)
//Yellow Glass
Player.addItemCreativeInv(180,1,0)
//Lime Green Glass
Player.addItemCreativeInv(181,1,0)
//Pink Glass
Player.addItemCreativeInv(182,1,0)
//Gray Glass
Player.addItemCreativeInv(183,1,0)
//Silver Glass
Player.addItemCreativeInv(184,1,0)
//Cyan Glass
Player.addItemCreativeInv(185,1,0)
//Purple Glass
Player.addItemCreativeInv(186,1,0)
//Blue Glass
Player.addItemCreativeInv(187,1,0)
//Brown Glass
Player.addItemCreativeInv(188,1,0)
//Green Glass
Player.addItemCreativeInv(189,1,0)
//Red Glass
Player.addItemCreativeInv(190,1,0)
//Black Glass
Player.addItemCreativeInv(191,1,0)

//Items
ModPE.setItem(368,"ender_pearl",0,"Ender Pearl");
ModPE.setItem(377,"blaze_powder",0,"Blaze Powder");
ModPE.setItem(381,"ender_eye",0,"Eye of Ender");
ModPE.setItem(415, "record_13", 0, "13 Disc", 1);
ModPE.setItem(416, "record_cat", 0, "Cat Disc", 1);
ModPE.setItem(417, "record_blocks", 0, "Blocks Disc", 1);
ModPE.setItem(418, "record_chirp", 0, "Chirp Disc", 1);
ModPE.setItem(419, "record_far", 0, "Far Disc", 1);
ModPE.setItem(420, "record_mall", 0, "Mall Disc", 1);
ModPE.setItem(421, "record_mellohi", 0, "Mellohi Disc", 1);
ModPE.setItem(422, "record_stal", 0, "Stal Disc", 1);
ModPE.setItem(423, "record_strad", 0, "Strad Disc", 1);
ModPE.setItem(424, "record_ward", 0, "Ward Disc", 1);
ModPE.setItem(425, "record_11", 0, "11 Disc", 1);
ModPE.setItem(426, "record_wait", 0, "Wait Disc", 1);

//Created Blocks
Block.defineBlock(130, "Ender Chest", [["ender_chest_top", 0], ["ender_chest_top" , 0], ["ender_chest_side" , 0], ["ender_chest_front", 0], ["ender_chest_side", 0], ["ender_chest_side", 0]],173);
Block.defineBlock(84, "Jukebox", [["jukebox_side", 0], ["jukebox_top", 0], ["jukebox_side", 0], ["jukebox_side", 0], ["jukebox_side", 0], ["jukebox_side", 0]]);
Block.defineBlock(33, "Piston", [["piston_side", 0], ["piston_top_normal", 0], ["piston_side", 0], ["piston_side", 0], ["piston_side", 0], ["piston_side", 0]]);
Block.defineBlock(176, "White Stained Glass", [["white_glass", 0], ["white_glass", 0], ["white_glass", 0], ["white_glass", 0], ["white_glass", 0], ["white_glass", 0]],20,false);
	Block.setRenderLayer(176,1);
Block.defineBlock(177, "Orange Stained Glass", [["orange_glass", 0], ["orange_glass", 0], ["orange_glass", 0], ["orange_glass", 0], ["orange_glass", 0], ["orange_glass", 0]],20,false);
	Block.setRenderLayer(177,1);
Block.defineBlock(178, "Magenta Stained Glass", [["magenta_glass", 0], ["magenta_glass", 0], ["magenta_glass", 0], ["magenta_glass", 0], ["magenta_glass", 0], ["magenta_glass", 0]],20,false);
	Block.setRenderLayer(178,1);
Block.defineBlock(179, "Light Blue Stained Glass", [["light_blue_glass", 0], ["light_blue_glass", 0], ["light_blue_glass", 0], ["light_blue_glass", 0], ["light_blue_glass", 0], ["light_blue_glass", 0]],20,false);
	Block.setRenderLayer(178,1);
Block.defineBlock(180, "Yellow Stained Glass", [["yellow_glass", 0], ["yellow_glass", 0], ["yellow_glass", 0], ["yellow_glass", 0], ["yellow_glass", 0], ["yellow_glass", 0]],20,false);
	Block.setRenderLayer(180,1);
Block.defineBlock(181, "Lime Green Stained Glass", [["lime_green_glass", 0], ["lime_green_glass", 0], ["lime_green_glass", 0], ["lime_green_glass", 0], ["lime_green_glass", 0], ["lime_green_glass", 0]],20,false);
	Block.setRenderLayer(181,1);
Block.defineBlock(182, "Pink Stained Glass", [["pink_glass", 0], ["pink_glass", 0], ["pink_glass", 0], ["pink_glass", 0], ["pink_glass", 0], ["pink_glass", 0]],20,false);
	Block.setRenderLayer(182,1);
Block.defineBlock(183, "Gray Stained Glass", [["gray_glass", 0], ["gray_glass", 0], ["gray_glass", 0], ["gray_glass", 0], ["gray_glass", 0], ["gray_glass", 0]],20,false);
	Block.setRenderLayer(183,1);
Block.defineBlock(184, "Silver Stained Glass", [["silver_glass", 0], ["silver_glass", 0], ["silver_glass", 0], ["silver_glass", 0], ["silver_glass", 0], ["silver_glass", 0]],20,false);
	Block.setRenderLayer(184,1);
Block.defineBlock(185, "Cyan Stained Glass", [["cyan_glass", 0], ["cyan_glass", 0], ["cyan_glass", 0], ["cyan_glass", 0], ["cyan_glass", 0], ["cyan_glass", 0]],20,false);
	Block.setRenderLayer(185,1);
Block.defineBlock(186, "Purple Stained Glass", [["purple_glass", 0], ["purple_glass", 0], ["purple_glass", 0], ["purple_glass", 0], ["purple_glass", 0], ["purple_glass", 0]],20,false);
	Block.setRenderLayer(186,1);
Block.defineBlock(187, "Blue Stained Glass", [["blue_glass", 0], ["blue_glass", 0], ["blue_glass", 0], ["blue_glass", 0], ["blue_glass", 0], ["blue_glass", 0]],20,false);
	Block.setRenderLayer(187,1);
Block.defineBlock(188, "Brown Stained Glass", [["brown_glass", 0], ["brown_glass", 0], ["brown_glass", 0], ["brown_glass", 0], ["brown_glass", 0], ["brown_glass", 0]],20,false);
	Block.setRenderLayer(188,1);
Block.defineBlock(189, "Green Stained Glass", [["green_glass", 0], ["green_glass", 0], ["green_glass", 0], ["green_glass", 0], ["green_glass", 0], ["green_glass", 0]],20,false);
	Block.setRenderLayer(189,1);
Block.defineBlock(190, "Red Stained Glass", [["red_glass", 0], ["red_glass", 0], ["red_glass", 0], ["red_glass", 0], ["red_glass", 0], ["red_glass", 0]],20,false);
	Block.setRenderLayer(190,1);
Block.defineBlock(191, "Black Stained Glass", [["black_glass", 0], ["black_glass", 0], ["black_glass", 0], ["black_glass", 0], ["black_glass", 0], ["black_glass", 0]],20,false);
	Block.setRenderLayer(191,1);

//Main functions
function selectLevelHook(){
	currentActivity.runOnUiThread(new java.lang.Runnable(){
		run: function(){
			try{
				var areSoundsMissing = false;
				var missingSoundsText = "";
				var checkSounds = [];
				for(var i = 0; i <= 11; i++){
					checkSounds[i] = new java.io.File(JUKEBOX_SONGS_PATH + discFiles[i]);
					if(!checkSounds[i].exists()){
						areSoundsMissing = true;
						missingSoundsText += discFiles[i];
						missingSoundsText += ", ";
					}
				}
				//REMOVE//
				if(areSoundsMissing)
					soundsMissingGUI(missingSoundsText.substring(0, missingSoundsText.length - 2));
			}
			catch(err){
				clientMessage("Error: " + err);
			}
		}
	});
}

//Destroy Block
function destroyBlock(x, y, z){
	var checkBlockJukebox = getJukeboxObjectFromXYZ(x, y, z);
	
	if(checkBlockJukebox != -1)
		checkBlockJukebox.removePlayingJukebox();
	
	if(getTile(x, y, z) == 33){
		preventDefault();
		Level.destroyBlock(x, y, z, false);
    	if(Level.getGameMode() == 0){
    		Level.dropItem(x, y, z, 0, 33, 1, 0);
		}
		else(Level.getGameMode() == 1);
	}

	if(getTile(x, y, z) == 28){
		preventDefault();
		Level.destroyBlock(x, y, z, false);
    	if(Level.getGameMode() == 0){
			Level.dropItem(x, y, z, 0, 28, 1, 0);
		}
		else(Level.getGameMode() == 1);
	}
	if(getTile(x, y, z) == 152){
		Level.addParticle(ParticleType.redstone, x+1, y, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x-1, y, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y+1, z, 0, 0, 0, 4);
		Level.addParticle(ParticleType.redstone, x, y-1, z, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y, z-1, 0, 0, 0, 2);
		Level.addParticle(ParticleType.redstone, x, y, z+1, 0, 0, 0, 2); 
	}
      
//Lamp off
	if(getTile(x, y, z) == 123){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
		if(Level.getGameMode() == 0){
			Level.dropItem(x, y, z, 0, 123, 1, 0);
		}
		else(Level.getGameMode() == 1);
	}

//Lamp on
	if(getTile(x, y, z) == 124){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}
	
//Glass
	if(getTile(x, y, z) == 176){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 177){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}
	
	if(getTile(x, y, z) == 178){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 179){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 180){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 181){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 182){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 183){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 184){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 185){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 186){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 187){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 188){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 189){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 190){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}

	if(getTile(x, y, z) == 191){
		preventDefault();
		Level.playSound(x, y, z, "random.glass",2,5);
		Level.destroyBlock(x, y, z, false);
	}
}

//Dropped Items
function deathHook(murderer, victim){
	if(Entity.getEntityTypeId(victim)==38){
		Level.dropItem(Entity.getX(victim), Entity.getY(victim),
		Entity.getZ(victim), 1, 368, 1, 0);
	}
	if(Entity.getEntityTypeId(victim)==36){
		Level.dropItem(Entity.getX(victim), Entity.getY(victim),
		Entity.getZ(victim), 1, 377, 1, 0);
	}
	//BETA
	if(Entity.getEntityTypeId(murderer)==80 && Entity.getEntityTypeId(victim)==33){
		//var random = Math.floor((Math.random() * 30) + 365);
		//if(random == 365){
		var randomDisc = Math.floor((Math.random() * 12) + 415);
		Level.dropItem(Entity.getX(victim), Entity.getY(victim),
		Entity.getZ(victim), 0, randomDisc, 1, 0);
	}
}

//Use Item
function useItem(x,y,z,i,b,s, itemDamage){
	if(b==130&&Level.getTile(x,y+1,z)==0){
		ex = x;
		ey = y;
		ez = z;
		Level.setTile(x,y,z,54);
		while(currentSlot<26){
			Level.setChestSlot(ex,ey,ez,currentSlot,eId[currentSlot],eData[currentSlot],eCount[currentSlot]);
			currentSlot++;
		}
		currentSlot=0;
	}

//Redstone
	if(getCarriedItem()==33){
		if(s===0){
			setTile(x,y-1,z,33,0);
		}
		if(s==1){
			setTile(x,y+1,z,33,1);
    
		}
	}

	//Block Define
	if(Level.getTile(x, y, z) == 84){
		preventDefault();

		//Block state
		var checkBlockJukebox = getJukeboxObjectFromXYZ(x, y, z);
		if(checkBlockJukebox != -1){
			checkBlockJukebox.removePlayingJukebox();
			return;
		}

		//Disk yield
		var carried = Player.getCarriedItem();
		if(carried >= 415 && carried <= 426){
			//jukebox: start playing
			try{
				jukeboxes[nOfJ] = new jukebox(Math.floor(x) + 0.5, Math.floor(y), Math.floor(z) + 0.5, carried);
				nOfJ++;
				if(Player.getCarriedItemCount() == 1)
					Player.clearInventorySlot(Player.getSelectedSlotId());
				else
					Entity.setCarriedItem(Player.getEntity(), carried, Player.getCarriedItemCount() - 1, 0);
			}
			//REMOVE//
			catch(err){
				ModPE.showTipMessage("Disk Sounds not found!");
			}
		}
	}
}

function blockEventHook(x,y,z,e,d){
	if(x==ex&&y==ey&&z==ez&&d==0){
		try{
			while(currentSlot<26){
				eId[currentSlot]=Level.getChestSlot(ex,ey,ez,currentSlot);
				eCount[currentSlot]=Level.getChestSlotCount(ex,ey,ez,currentSlot);
				eData[currentSlot]=Level.getChestSlotData(ex,ey,ez,currentSlot);
				Level.setChestSlot(ex,ey,ez,currentSlot,0,0,0);
				currentSlot++;
			}
			currentSlot=0;
			Level.setTile(ex,ey,ez,130);
		}
		catch(err){
			print(err);
		}
	}
}

//On new level
function newLevel(){
	pathsave = android.os.Environment.getExternalStorageDirectory().getPath() + "/games/com.mojang/minecraftworlds/" + Level.getWorldName() + "/";
	try {
		var file = new java.io.File(pathsave + "enderchestId.txt");
		var fos = new java.io.FileInputStream(file);
		var str = new java.lang.StringBuilder();
		var ch;
		while ((ch = fos.read()) != -1);
		str.append(java.lang.Character(ch));
		var pre = str.toString();
		eId.length=0;
		pre.split(",").forEach(function(e){eId.push(parseInt(e,10))});
		fos.close();
	} catch (err) {}
	
	try {
		var file = new java.io.File(pathsave + "enderchestData.txt");
		var fos = new java.io.FileInputStream(file);
		var str = new java.lang.StringBuilder();
		var ch;
		while ((ch = fos.read()) != -1);
		str.append(java.lang.Character(ch));
		var pre = str.toString();
		eData.length=0;
		pre.split(",").forEach(function(e){eData.push(parseInt(e,10))});
		fos.close();
	} catch (err) {}
	
	try {
		var file = new java.io.File(pathsave + "enderchestCount.txt");
		var fos = new java.io.FileInputStream(file);
		var str = new java.lang.StringBuilder();
		var ch;
		while ((ch = fos.read()) != -1)
		str.append(java.lang.Character(ch));
		var pre = str.toString();
		eCount.length=0;
		pre.split(",").forEach(function(e){eCount.push(parseInt(e,10))});
		fos.close();
	} catch (err) {}
}

function newLevel(){
	if(initCreativeItems){
		Player.addItemCreativeInv(84, 1);
		for(var i = 415; i <= 426; i++)
			Player.addItemCreativeInv(i, 1);
		initCreativeItems = false;
	}
}

//On exit
function leaveGame(){
	java.io.File(pathsave).mkdirs();
	
	var newFile = new java.io.File(pathsave, "enderchestId.txt");
		newFile.createNewFile();
	var outWrite = new java.io.OutputStreamWriter(new java.io.FileOutputStream(newFile));
		outWrite.append(eId.toString());
		outWrite.close();
	
	var newFile2 = new java.io.File(pathsave, "enderchestData.txt");
	 newFile2.createNewFile();
	var outWrite2 = new java.io.OutputStreamWriter(new java.io.FileOutputStream(newFile2));
	 outWrite2.append(eData.toString());
	 outWrite2.close();
	
	var newFile3 = new java.io.File(pathsave, "enderchestCount.txt");
	 newFile3.createNewFile();
	var outWrite3 = new java.io.OutputStreamWriter(new java.io.FileOutputStream(newFile3));
	 outWrite3.append(eCount.toString());
	 outWrite3.close();
}

//Review//
function leaveGame(){
	for(var i in jukeboxes)
		jukeboxes[i].player.reset();

	nOfJ = 0;
	jukeboxes = [];

	nowPlayingMessageBool = false;
	countdownForNowPlayingMessage = 0;
	timesChanged = 0;
	currentColor = 0;
	nowPlayingMessage = "";
}

//Location
function modTick(){
	for(var i in jukeboxes){
		jukeboxes[i].countdown++;
		if(jukeboxes[i].countdown == 10){
			jukeboxes[i].countdown = 0;
			var distancePJ = Math.sqrt( (Math.pow(jukeboxes[i].x - Player.getX(), 2)) + (Math.pow(jukeboxes[i].y - Player.getY(), 2)) + (Math.pow(jukeboxes[i].z - Player.getZ(), 2) ));
			if(distancePJ > MAX_LOGARITHMIC_VOLUME)
				jukeboxes[i].player.setVolume(0.0, 0.0);
			else{
				var volume = 1 - (Math.log(distancePJ) / Math.log(MAX_LOGARITHMIC_VOLUME));
				jukeboxes[i].player.setVolume(volume, volume);
			}
		}
	}
	
	if(nowPlayingMessageBool){
		if((countdownForNowPlayingMessage % 5) == 0){
			if(timesChanged == 1){
				ModPE.showTipMessage(" ");
				countdownForNowPlayingMessage = 0;
				timesChanged = 0;
				currentColor = 0;
				nowPlayingMessageBool = false;
			}
			else{
				ModPE.showTipMessage("§" + currentColor.toString(16) + nowPlayingMessage);
				if(currentColor  == 15){
					timesChanged++;
					currentColor = 0;
				}
				else
					currentColor++;
			}
		}
		countdownForNowPlayingMessage++;
	}
//	BETA
//	if(Player.getCarriedItem()==261){
//		tick=60;
//		useable=false;
//	}
//	else{
//		if(tick>0)
//			tick--;
//	}
//	if(tick==0)
//		useable=true;
}

//GUI (off)
function jukebox(x, y, z, disc){
	this.x = x;
	this.y = y;
	this.z = z;
	this.countdown = 0;
	this.player = new android.media.MediaPlayer();
	this.disc = disc;
	this.player.reset();
	this.player.setDataSource(JUKEBOX_SONGS_PATH + getDataSourceFromDisc(disc));
	this.player.prepare();
	this.player.setVolume(1.0, 1.0);
	this.player.setOnCompletionListener(new android.media.MediaPlayer.OnCompletionListener(){
			onCompletion: function(){
				getJukeboxObjectFromXYZ(x, y, z).removePlayingJukebox();
			}
		});
	this.player.start();

	nowPlayingMessageBool = true;
	nowPlayingMessage = "Now playing: " + Item.getDiscName(disc);
	countdownForNowPlayingMessage = 0;

//Review//
	this.removePlayingJukebox = function(){
		this.ejectDisc();
		this.player.reset();
		jukeboxes.splice(jukeboxes.indexOf(this), 1);
		nOfJ--;
	}

	this.ejectDisc = function(){
		Level.dropItem(this.x, this.y + 1, this.z, 0, this.disc, 1, 0);
	}
}

function getJukeboxObjectFromXYZ(x, y, z){
	for(var i in jukeboxes)
		if(Math.floor(jukeboxes[i].x) == Math.floor(x) && Math.floor(jukeboxes[i].y) == Math.floor(y) && Math.floor(jukeboxes[i].z) == Math.floor(z))
			return jukeboxes[i];
	return -1;
}

function getDataSourceFromDisc(disc){
	for(var i in discFiles)
		if(disc - 415 == i)
			return discFiles[i];
}

Item.getDiscName = function(disc){
	for(var i in discNames)
		if(disc - 415 == i)
			return discNames[i];
}

//Destroy
Block.setDestroyTime(28, 0.75);
Block.setDestroyTime(33, 0.75);
Block.setDestroyTime(130, 22.5);
Block.setDestroyTime(84, 2);

//Explosion Resist
Block.setExplosionResistance(84, 30);
Block.setExplosionResistance(130, 3000)

//Crafting

//Ender Chest
Item.addShapedRecipe(130,1,0,[
	"ooo",
	"oeo",
	"ooo"],[
	"o",49,0,
	"e",381,0]);

//Eye of Ender
Item.addShapedRecipe(381,1,0,[
	" b ",
	" p ",
	"   "],[
	"b",377,0,
	"p",368,0]);
	
//Jukebox
Item.addShapedRecipe(84, 1, 0, [
	"www",
	"wdw",
	"www"], ["w", 5, 0, "d", 264, 0]);